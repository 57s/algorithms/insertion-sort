function insertionSort(array) {
	for (let i = 1; i < array.length; i++) {
		const buffer = array[i];
		let j = i - 1;

		for (; j >= 0 && array[j] > buffer; j--) {
			array[j + 1] = array[j];
		}

		array[j + 1] = buffer;
	}
	return array;
}

export { insertionSort };
