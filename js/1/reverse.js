function insertionSort(array) {
	for (let i = array.length - 2; i >= 0; i--) {
		const buffer = array[i];
		let j = i + 1;

		while (j < array.length && array[j] < buffer) {
			array[j - 1] = array[j];
			j++;
		}

		array[j - 1] = buffer;
	}

	return array;
}

export { insertionSort };
