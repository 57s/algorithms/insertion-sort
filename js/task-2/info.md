# Найти медиану

**Медиана** - это число, которое находится в середине ряда чисел.
Если в последнем четное количество показателей,
медиана - это среднее арифметическое двух средних чисел.
