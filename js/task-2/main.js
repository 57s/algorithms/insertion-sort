function medianSearch(array) {
	for (let i = 1; i < array.length; i++) {
		const buffer = array[i];
		let j = i - 1;

		while (j >= 0 && array[j] > buffer) {
			array[j + 1] = array[j];
			j--;
		}

		array[j + 1] = buffer;
	}

	if (array.length % 2 !== 0) {
		return array[array.length / 2];
	} else {
		let left = Math.floor((array.length - 1) / 2);
		let right = left + 1;

		return (array[left] + array[right]) / 2;
	}
}

export { medianSearch };
