// Не подходит для сортировки слов с 'Ё'

function insertionSort(array) {
	for (let i = 1; i < array.length; i++) {
		const buffer = array[i];
		let j = i - 1;

		while (j >= 0 && array[j] > buffer) {
			array[j + 1] = array[j];
			j--;
		}

		array[j + 1] = buffer;
	}
	return array;
}

export { insertionSort };
