// Учитывает 'Ё'

function alphabetSort(arr) {
	for (let i = 1; i < arr.length; i++) {
		const temp = arr[i];
		let j = i - 1;

		while (j >= 0 && arr[j].localeCompare(temp, 'ru') > 0) {
			arr[j + 1] = arr[j];
			j--;
		}

		arr[j + 1] = temp;
	}

	return arr;
}

export { alphabetSort };
